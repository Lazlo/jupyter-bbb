#!/bin/bash

set -e
set -u

install_deps() {
	# Check xz-utils and wget are installed
	local -r deps="wget xz-utils parted e2fsprogs"
	set +e
	dpkg -s wget > /dev/null
	local rc=$?
	set -e
	if [ $rc != 0 ]; then
		apt-get -q install -y $deps
	fi
}

fetch_and_write_img() {
	local -r cache_dir="$1"
	local -r archive="$2"
	local -r url="$3"
	local -r disk="$4"

	mkdir -p $cache_dir
	# Fetch image
	if [ ! -f $cache_dir/$archive ]; then
		wget -O $cache_dir/$archive $url
	fi
	# Write image to block device
	xzcat $cache_dir/$archive | dd of=$disk status=progress
	# Force writing to block device
	sync
}

grow_root_partiton() {
	local -r disk="$1"

	# Delete root partition from partition table
	sfdisk --delete $disk 1
	# Create new root partition
	echo "start=8192 type=0x83" | sfdisk $disk
	# Run fsck
	e2fsck -f -y ${disk}1
	# Resize file system in root partition
	resize2fs ${disk}1
}

main() {
	local -r archive_v9_5="bone-debian-9.5-iot-armhf-2018-10-07-4gb.img.xz"
	local -r archive_v9_9="bone-debian-9.9-iot-armhf-2019-08-03-4gb.img.xz"
	local -r archive="$archive_v9_9"
	local -r url="https://debian.beagleboard.org/images/$archive"
	local -r cache_dir="cache"
	if [ $# != 1 ]; then
		echo "USAGE: $(basename $0) disk"
		echo ""
		echo "NOTE Run  'lsbld -d' command to identify name of desired block device."
		exit 1
	fi
	local -r disk="$1"
	if [ $UID != 0 ]; then
		echo "ERROR: Run as root!"
		exit 1
	fi
	install_deps
	fetch_and_write_img "$cache_dir" "$archive" "$url" "$disk"
	grow_root_partition "$disk"
}

main $@
