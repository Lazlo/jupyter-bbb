#!/bin/bash

set -e
set -u

main() {
	if [ $# != 1 ]; then
		echo "USAGE: $(basename $0) uplink-iface"
		exit 1
	fi
	local -r uplink_iface="$1"
	if [ $UID != 0 ]; then
		echo "ERROR: Run as root!"
		exit 1
	fi
	# enable forwarding
	sed -i -E 's/#(net\.ipv4\.ip_forward)=[0-1]/\1=1/' /etc/sysctl.conf
	sysctl -q -p
	# insert NAT masquerading rule
	iptables -t nat -A POSTROUTING -o $uplink_iface -j MASQUERADE
}

main $@
