Run all playbooks on all hosts in the inventory and use sudo on the remote.

```
ansible-playbook -i hosts site.yml -K
```
